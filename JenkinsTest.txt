﻿*** Settings ***
Documentation     This is your first test!
Resource          common.txt
Library           Selenium2Library    20

*** Test Cases ***
Go To Zervant [Documentation] Get this to work in Jenkins.
    Open Browser    http://testprod.zervant.com/login.html#/state/0/
    Maximize Browser Window
    Get Title
    BuiltIn.Sleep    3s
    Wait Until Page Contains Element    xpath=/html/body/div[2]/div/div[2]/div[3]/div/a
    Page Should Contain Element    xpath=/html/body/div[2]/div/div[2]/div[3]/div/a
    Element Should Be Visible    xpath=/html/body/div[2]/div/div[1]/div/div/div[1]/a
    Input Text    username    dasko@zervant.com
    Input Text    password    Ventrad0
    Click Element    xpath=/html/body/div[2]/div/div[2]/div[3]/div/a
    Wait Until Page Contains    until
    Get Title
    BuiltIn.Sleep    2s
    Page Should Contain    Developer
    Page Should Contain    Valid
    Element Should Be Visible    xpath=/html/body/div[1]/div[2]/div[1]/div/div[2]/div[1]/div/div[4]/div/a/span/div
	Close Browser
﻿//Home.js: Testprod goes to the Homepage!  THIS WORKS!!

casper.test.begin('Initial TestProd test', 6, function suite(test) {
	casper.start('http://185.26.48.241/login.html#/state/0/', function() {
		this.wait(4000, function() {
			this.echo("\nWaited for 5 seconds.");
			this.test.assertTitle('Zervant');		//Zervant
			//this.test.assertExists('form[action="/search"]');
			this.test.assertTextExists('address', 'Page has expected text: address');
			this.test.assertExists('input[name=username]');
			this.test.assertExists('input[name=password]');
			this.test.assertExists({
					type:	'xpath',
					path:	'/html/body/div[2]/div/div[2]/div[3]/div/a'
				}, 'the Login button exists');
		this.sendKeys('input[name="username"]', 'smoketest1@test.com');
		this.sendKeys('input[name="password"]', 'Ventrad0');
		this.capture('HomeAWSLogin.png');
		this.clickLabel('Login', 'a');
		this.wait(7000, function() {
			this.echo("\nWaited for Home to appear.");			//This is how you do wait in Javascript
			this.test.assertTextExists('smoketest1@test.com', 'Appears in Home page');
		});
		});
	});
});

casper.run(function() {
	this.capture('HomeAWS1.png');
    this.exit();
});